"""
Tests for the workouts application.
"""
from django.test import RequestFactory, TestCase
from datetime import datetime
from users.models import User
from workouts.models import Workout, Exercise, ExerciseInstance
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly

# Create your tests here.


class WorkoutPermissionsTestCase(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(
            username='testUser1', password='testPassword1')
        self.user2 = User.objects.create(
            username='testUser2', password='testPassword2', coach=self.user1)

        self.workout1 = Workout.objects.create(name="testWorkout1", date=datetime.now(
        ), notes="This is a test", owner=self.user1, visibility="PU")
        self.workout2 = Workout.objects.create(name="testWorkout2", date=datetime.now(
        ), notes="This is a test", owner=self.user2, visibility="PR")

        self.exercise = Exercise.objects.create(
            name="testExercise", description="This is a test")

        self.exerciseInstance1 = ExerciseInstance.objects.create(
            workout=self.workout1, exercise=self.exercise, sets=1, number=1)
        self.exerciseInstance2 = ExerciseInstance.objects.create(
            workout=self.workout2, exercise=self.exercise, sets=1, number=1)

        self.factory = RequestFactory()
        self.request1 = self.factory.get("/workout")
        self.request1.user = self.user1
        self.request1.method = "POST"
        self.request1.data = {"workout": '/workout/1/'}

        self.request2 = self.factory.get("/")
        self.request2.user = self.user2

        self.isOwner = IsOwner()
        self.IsOwnerOfWorkout = IsOwnerOfWorkout()
        self.isCoachAndVisibleToCoach = IsCoachAndVisibleToCoach()
        self.isCoachofWorkoutAndVisibleToCoach = IsCoachOfWorkoutAndVisibleToCoach()
        self.isPublic = IsPublic()
        self.isWorkoutPublic = IsWorkoutPublic()
        self.isReadOnly = IsReadOnly()

    # def test_is_owner(self):
    #     self.assertTrue(self.isOwner.has_object_permission(
    #         self.request1, "view", self.workout1))

    #     self.assertFalse(self.isOwner.has_object_permission(
    #         self.request2, "view", self.workout1))

    # def test_is_owner_of_workout(self):
    #     self.assertTrue(
    #         self.IsOwnerOfWorkout.has_permission(self.request1, "view"))

    #     self.assertTrue(self.IsOwnerOfWorkout.has_object_permission(
    #         self.request1, "view", self.exerciseInstance1))
    #     self.assertFalse(self.IsOwnerOfWorkout.has_object_permission(
    #         self.request2, "view", self.exerciseInstance1))

    # def test_is_coach_and_visible_to_coach(self):
    #     self.assertTrue(self.isCoachAndVisibleToCoach.has_object_permission(
    #         self.request1, "view", self.workout2))
    #     self.assertFalse(self.isCoachAndVisibleToCoach.has_object_permission(
    #         self.request1, "view", self.workout1))

    # def test_is_coach_of_workout_and_visible_to_coach(self):
    #     self.assertTrue(self.isCoachofWorkoutAndVisibleToCoach.has_object_permission(
    #         self.request1, "view", self.exerciseInstance2))
    #     self.assertFalse(self.isCoachofWorkoutAndVisibleToCoach.has_object_permission(
    #         self.request1, "view", self.exerciseInstance1))

    # def test_is_public(self):
    #     self.assertTrue(self.isPublic.has_object_permission(
    #         self.request1, "view", self.workout1))
    #     self.assertFalse(self.isPublic.has_object_permission(
    #         self.request1, "view", self.workout2))

    # def test_is_workout_public(self):
    #     self.assertTrue(self.isWorkoutPublic.has_object_permission(
    #         self.request1, "view", self.exerciseInstance1))
    #     self.assertFalse(self.isWorkoutPublic.has_object_permission(
    #         self.request1, "view", self.exerciseInstance2))

    # def test_is_read_only(self):
    #     self.assertTrue(self.isReadOnly.has_object_permission(
    #         self.request2, "view", self.user1))
    #     self.assertFalse(self.isReadOnly.has_object_permission(
    #         self.request1, "view", self.user1))
