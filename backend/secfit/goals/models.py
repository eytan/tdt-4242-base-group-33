import os
from unicodedata import name
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.auth import get_user_model
import datetime


class OverwriteStorage(FileSystemStorage):
    """Filesystem storage for overwriting files. Currently unused."""

    def get_available_name(self, name, max_length=None):
        """https://djangosnippets.org/snippets/976/
        Returns a filename that's free on the target storage system, and
        available for new content to be written to.

        Args:
            name (str): Name of the file
            max_length (int, optional): Maximum length of a file name. Defaults to None.
        """
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))


class Goal(models.Model):
    TIMING = "Timing"
    REPS = "Reps"
    BODYWEIGHT = "Bodyweight"
    WEIGHTLIFTING = "Weightlifting"
    GOAL_CHOICES = [
        (TIMING, "Timing"),
        (REPS, "Reps"),
        (BODYWEIGHT, "Bodyweight"),
        (WEIGHTLIFTING, "Weightlifting"),
    ]

    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    typeGroup = models.CharField(
        max_length=13, choices=GOAL_CHOICES, default=TIMING)
    target = models.TextField()
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="goals"
    )

    class Meta:
        ordering = ["date"]

    def __str__(self):
        return self.name


def goal_directory_path(instance, filename):
    """Return path for which goal files should be uploaded on the web server

    Args:
        instance (GoalFile): GoalFile instance
        filename (str): Name of the file

    Returns:
        str: Path where workout file is stored
    """
    return f"goals/{instance.goal.id}/{filename}"


class GoalFile(models.Model):
    """Django model for file associated with a goal. Basically a wrapper.

    Attributes:
        goal:    The goal for which this file has been uploaded
        owner:   The user who uploaded the file
        file:    The actual file that's being uploaded
    """

    goal = models.ForeignKey(
        Goal, on_delete=models.CASCADE, related_name="files")
    owner = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="goal_files"
    )
    file = models.FileField(upload_to=goal_directory_path)
