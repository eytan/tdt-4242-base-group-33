from rest_framework import generics, mixins
from rest_framework import permissions

from rest_framework.parsers import (
    JSONParser,
)
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.db.models import Q
from rest_framework import filters
from goals.parsers import MultipartJsonParser
from goals.permissions import (
    IsOwner,
    IsReadOnly,
)
from goals.mixins import CreateListModelMixin
from goals.models import Goal, GoalFile
from goals.serializers import GoalSerializer, GoalFileSerializer
from django.core.exceptions import PermissionDenied
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.response import Response
import json
from collections import namedtuple
import base64
import pickle
from django.core.signing import Signer


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "users": reverse("user-list", request=request, format=format),
            "goals": reverse("goal-list", request=request, format=format),
            "goal-files": reverse("goal-file-list", request=request, format=format),
        }
    )


class GoalList(
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        generics.GenericAPIView
):
    serializer_class = GoalSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    parser_classes = [
        MultipartJsonParser,
        JSONParser,
    ]

    filter_backends = [filters.OrderingFilter]

    ordering_fields = ["name", "typeGroup", "target"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        qs = Goal.objects.none()
        if self.request.user:
            qs = Goal.objects.filter(Q(owner=self.request.user)).distinct()
        return qs


class GoalDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    queryset = Goal.objects.all()
    serializer_class = GoalSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & IsOwner
    ]

    parser_classes = [
        MultipartJsonParser,
        JSONParser
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class GoalFileDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):

    queryset = GoalFile.objects.all()
    serializer_class = GoalFileSerializer
    permission_classes = [
        permissions.IsAuthenticated
        & IsOwner
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
