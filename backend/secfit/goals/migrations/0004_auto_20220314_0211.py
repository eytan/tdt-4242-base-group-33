# Generated by Django 3.1 on 2022-03-14 02:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goals', '0003_auto_20220314_0131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goal',
            name='date',
            field=models.DateTimeField(),
        ),
    ]
