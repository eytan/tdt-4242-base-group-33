from django.urls import path, include
from goals import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = format_suffix_patterns(
    [
        path("", views.api_root),
        path("api/goals/", views.GoalList.as_view(), name="goal-list"),
        path("api/goals/<int:pk>/", views.GoalDetail.as_view(), name="goal-detail"),
        path("", include("users.urls")),
    ]
)
