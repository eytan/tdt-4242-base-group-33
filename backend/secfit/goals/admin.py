from django.contrib import admin

# Register your models here.
from .models import Goal, GoalFile

admin.site.register(Goal)
admin.site.register(GoalFile)
