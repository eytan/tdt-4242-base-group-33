from rest_framework import serializers
from rest_framework.serializers import HyperlinkedRelatedField
from goals.models import Goal, GoalFile


class GoalSerializer(serializers.HyperlinkedModelSerializer):

    owner_username = serializers.SerializerMethodField()

    class Meta:
        model = Goal
        fields = ["url", "id", "name", "date", "typeGroup",
                  "target", "owner", "owner_username", "files"]
        extra_kwargs = {"owner": {"read_only": True}}

    def create(self, validated_data):
        """Custom logic for creating MealFiles, and a Meal.

        This is needed to iterate over the files, since this serializer is nested.

        Args:
            validated_data: Validated files

        Returns:
            Goal: A newly created Goal
        """
        files_data = []
        if "files" in validated_data:
            files_data = validated_data.pop("files")

        goal = Goal.objects.create(**validated_data)

        for file_data in files_data:
            GoalFile.objects.create(
                goal=goal, owner=goal.owner, file=file_data.get("file")
            )

        return goal

    def update(self, instance, validated_data):
        """Custom logic for updating a Meal.

        This is needed because each object in files must be iterated
        over and handled individually.

        Args:
            instance (Meal): Current Meal object
            validated_data: Contains data for validated fields

        Returns:
            Meal: Updated Meal instance
        """

        instance.name = validated_data.get("name", instance.name)
        instance.date = validated_data.get("date", instance.date)
        instance.typeGroup = validated_data.get(
            "typeGroup", instance.typeGroup)
        instance.target = validated_data.get("target", instance.target)
        instance.save()

        # Handle GoalFiles

        if "files" in validated_data:
            files_data = validated_data.pop("files")
            files = instance.files

            for file, file_data in zip(files.all(), files_data):
                file.file = file_data.get("file", file.file)

            # If new files have been added, creating new MealFiles
            if len(files_data) > len(files.all()):
                for i in range(len(files.all()), len(files_data)):
                    GoalFile.objects.create(
                        goal=instance,
                        owner=instance.owner,
                        file=files_data[i].get("file"),
                    )
            # Else if files have been removed, delete MealFiles
            elif len(files_data) < len(files.all()):
                for i in range(len(files_data), len(files.all())):
                    files.all()[i].delete()

        return instance

    def get_owner_username(self, obj):
        """Returns the owning user's username

        Args:
            obj (Meal): Current Meal

        Returns:
            str: Username of owner
        """
        return obj.owner.username


class GoalFileSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")
    goal = HyperlinkedRelatedField(
        queryset=Goal.objects.all(), view_name="goal-detail", required=False
    )

    class Meta:
        model = GoalFile
        fields = ["url", "id", "owner", "file", "goal"]

    def create(self, validated_data):
        return GoalFile.objects.create(**validated_data)
