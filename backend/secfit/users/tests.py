from django.test import TestCase
from users.models import User
from users.serializers import UserSerializer
from rest_framework.exceptions import ValidationError
from rest_framework import serializers

# Create your tests here.


class UserSerializerTestCase(TestCase):
    def setUp(self):
        self.data = {
            "username": "usertest1",
            "email": "usertest@hotmail.com",
            "password": "Abcd123",
            "password1": "Abcd123",
            "phone_number": "9221 2219",
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Herman Kragsveg 10",
        }

    def test_create(self):
        serializer = UserSerializer()

        new_user = serializer.create(validated_data=self.data)
        self.assertEqual(new_user.username, self.data["username"])
        self.assertEqual(new_user.email, self.data["email"])
        self.assertEqual(new_user.phone_number, self.data["phone_number"])
        self.assertEqual(new_user.country, self.data["country"])
        self.assertEqual(new_user.city, self.data["city"])
        self.assertEqual(new_user.street_address, self.data["street_address"])

        stored_user = User.objects.get(username=self.data["username"])
        self.assertEqual(stored_user.username, self.data["username"])
        self.assertEqual(stored_user.email, self.data["email"])
        self.assertEqual(stored_user.phone_number, self.data["phone_number"])
        self.assertEqual(stored_user.country, self.data["country"])
        self.assertEqual(stored_user.city, self.data["city"])
        self.assertEqual(stored_user.street_address,
                         self.data["street_address"])

    def test_validate_password(self):
        # serializer = UserSerializer(data={"password": "aa"})
        # with self.assertRaises(serializers.ValidationError):
        #     serializer.validate_password(value="a")
        self.serializer = UserSerializer()
        self.assertEquals(self.serializer.validate_password(
            self.data["password"]), self.data["password"])
        self.assertNotEqual(self.serializer.validate_password(
            "TestingWrongPW"), self.data["password"])

    def test_validate_password_valid(self):
        pwd = "!Abcde123"

        serializer = UserSerializer(data={"password": pwd, "password1": pwd})

        try:
            serializer.validate_password(value="b")
        except serializers.ValidationError:
            self.fail("raise ValidationError")
