async function fetchExerciseTypes(request) {
    let response = await sendRequest("GET", `${HOST}/api/exercises/`);

    if (response.ok) {
        let data = await response.json();

        let exercises = data.results;
        let container = document.getElementById('div-content');
        let exerciseTemplate = document.querySelector("#template-exercise");
        
        //This will take the individual entries from the database and display it on the exercises.html page
        exercises.forEach(exercise => {
            const exerciseAnchor = exerciseTemplate.content.firstElementChild.cloneNode(true);
            exerciseAnchor.href = `exercise.html?id=${exercise.id}`; //URL for each individual exercise 

            // const input = exerciseAnchor.querySelector("input");
            // input.id = exercise.id;
            // input.value = exercise.id;

            const h5 = exerciseAnchor.querySelector("h5");
            h5.textContent = exercise.name;

            const p = exerciseAnchor.querySelector("p");
            p.textContent = exercise.description;   

            container.appendChild(exerciseAnchor);
        });
    }

    return response;
}

function createExercise() {
    window.location.replace("exercise.html");
}

// // testing
// function changed(termsCheckBox){
//     //If the checkbox has been checked
//     if(termsCheckBox.checked){
//         document.getElementById("btn-delete-test").disabled = false;
//     } else{
//         document.getElementById("btn-delete-test").disabled = true;
//     }
// }


// // testing
// async function deleteExercise(id) {
//     let response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
//     if (!response.ok) {
//         let data = await response.json();
//         let alert = createAlert(`Could not delete exercise ${id}`, data);
//         document.body.prepend(alert);
//     } else {
//         window.location.replace("exercises.html");
//     }
// }

// function getIdsToDelete(){
//    //let exerciseTemplate = document.querySelector("#template-exercise");

//     //let cbs = document.querySelectorAll('.checkbox').checked;

//     const list_test = []
//     var markedCheckbox = document.querySelectorAll('input[type="checkbox"]:checked');  
//     console.log(markedCheckbox)
   
//     for (i=0; i<markedCheckbox.length; i++) {  
//         id = markedCheckbox[i].id
//         list_test.push(id);
//         deleteExercise(id);
//         // deleteButton.addEventListener("click", (async (id) => await deleteExercise(id)).bind(undefined, exerciseId));
//     }  
   

// }

window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-exercise");
    createButton.addEventListener("click", createExercise);

    // //testing
    // let deleteButton = document.querySelector('#btn-delete-test');
    // deleteButton.addEventListener("click", getIdsToDelete);

    let response = await fetchExerciseTypes();
    
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve exercise types!", data);
        document.body.prepend(alert);
    }
});
