async function fetchGoals(request) {
    let response = await sendRequest("GET", `${HOST}/api/goals/`);

    if (response.ok) {
        let data = await response.json();
        
        let goals = data.results;
        let container = document.getElementById('div-content');
        //let goalTemplate = document.querySelector("#template-goals");


        goals.forEach(goal => {
            let goalTemplate = document.querySelector("#template-goals");
            const goalAnchor = goalTemplate.content.firstElementChild.cloneNode(true);
            goalAnchor.href = `create-goal.html?id=${goal.id}`;

            const h5 = goalAnchor.querySelector("h5");
            h5.textContent = goal.name;

            // const p = goalAnchor.querySelector("p");
            // p.textContent = goal.target;   

            const p = goalAnchor.getElementsByTagName('p');
            p[0].textContent = "Target to hit: " + goal.target;
            p[1].textContent = "Target date: " + goal.date;
            p[2].textContent = "Type: " + goal.typeGroup;
    
            
            /*
            let table = aMeal.querySelector("table");
            let rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll("td")[1].textContent = goal.typegroup; // Goal Type
            rows[1].querySelectorAll("td")[1].textContent = goal.target; // Target
            */
           
            container.appendChild(goalAnchor);
        });
    
    }


    return response;
}

function createGoal() {
    window.location.replace("create-goal.html");
}

function deleteGoal(){
    window.location.replace("delete-goal.html");
    // deleteButton.removeEventListener("click", deleteGoal);
}


window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-goal");
    let deleteButton = document.querySelector("#btn-delete-goal");

    createButton.addEventListener("click", createGoal);
    deleteButton.addEventListener("click", deleteGoal);

    let response = await fetchGoals();
    
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve your goals!", data);
        document.body.prepend(alert);
    }
});