let cancelGoalButton;
let okGoalButton;
let deleteGoalButton;
let editGoalButton;


async function retrieveGoal(id) {  
    let goalData = null;
    let response = await sendRequest("GET", `${HOST}/api/goals/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve your goal data!", data);
        document.body.prepend(alert);
    } else {
        goalData = await response.json();
        let form = document.querySelector("#form-create-goal");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            let newVal = goalData[key];
            if (key == "date") {
                // Creating a valid datetime-local string with the correct local time
                let date = new Date(newVal);
                date = new Date(date.getTime() - (date.getTimezoneOffset() * 60 * 1000)).toISOString(); // get ISO format for local time
                newVal = date.substring(0, newVal.length - 1);    // remove Z (since this is a local time, not UTC)
            }
            if (key != "files") {
                input.value = newVal;
            }
        }

        let input = form.querySelector("select:disabled");
        // files
        let filesDiv = document.querySelector("#uploaded-files");
        for (let file of goalData.files) {
            let a = document.createElement("a");
            a.href = file.file;
            let pathArray = file.file.split("/");
            a.text = pathArray[pathArray.length - 1];
            a.className = "me-2";
            filesDiv.appendChild(a);
        }
    }
    return goalData;     
}

// function handleCancelDuringGoalEdit() {
//     location.reload();
// }

// function handleEditGoalButtonClick() {
    
//     setReadOnly(false, "#form-create-goal");
//     document.querySelector("#inputOwner").readOnly = true;  // owner field should still be readonly 


//     editGoalButton.className += " hide"; // The edit button should be hidden when in edit mode
//     okGoalButton.className = okGoalButton.className.replace(" hide", ""); // The ok button should not be hidden when in edit mode
//     cancelGoalButton.className = cancelGoalButton.className.replace(" hide", ""); // See above
//     deleteGoalButton.className = deleteGoalButton.className.replace(" hide", ""); // See above
//     cancelGoalButton.addEventListener("click", handleCancelDuringGoalEdit);

// }

// async function deleteGoal(id) {
//     let response = await sendRequest("DELETE", `${HOST}/api/goals/${id}/`);
//     if (!response.ok) {
//         let data = await response.json();
//         let alert = createAlert(`Could not delete this goal. ID: ${id}!`, data);
//         document.body.prepend(alert);
//     } else {
//         window.location.replace("goals.html");
//     }
// }

// async function updateGoal(id) {
//     let submitForm = generateGoalForm();

//     let response = await sendRequest("PUT", `${HOST}/api/goals/${id}/`, submitForm, "");
//     if (!response.ok) {
//         let data = await response.json();
//         let alert = createAlert("Could not update your goal! :-( ", data);
//         document.body.prepend(alert);
//     } else {
//         location.reload();
//     }
// }

function generateGoalForm() {
    let form = document.querySelector("#form-create-goal");

    let formData = new FormData(form);
    let submitForm = new FormData();

    submitForm.append("name", formData.get('name'));
    if (!Date.parse(formData.get('date'))){
        return null;
    }
    else {
        let date = new Date(formData.get('date')).toISOString();
        submitForm.append("date", date);
        submitForm.append("typeGroup", formData.get("typeGroup"));
        submitForm.append("target", formData.get("target"));

        for (let key of submitForm.keys()) {
            console.log(key);
        }
        return submitForm;
    }
    
    
    
}

async function createGoal() {
    let submitForm = generateGoalForm();

    let response = await sendRequest("POST", `${HOST}/api/goals/`, submitForm, "");

    if (response.ok) {
        window.location.replace("goals.html");
    } else {
        console.log(submitForm)
        let data = await response.json();
        let alert = createAlert("Could not create new goal", data);
        document.body.prepend(alert);
    }
}

function backToGoalsMainPage() {
    window.location.replace("goals.html");
}

function backToDeleteGoalsPage() {
    window.location.replace("delete-goal.html");
}

function getReferrer(){
    var previousPage = document.referrer;
    if (previousPage == null)
        return null;
    else
        return previousPage;
}


window.addEventListener("DOMContentLoaded", async () => {
    cancelGoalButton = document.querySelector("#btn-cancel-goal");
    okGoalButton = document.querySelector("#btn-ok-goal");
    // deleteGoalButton = document.querySelector("#btn-delete-goal");
    // editGoalButton = document.querySelector("#btn-edit-goal");

    const urlParams = new URLSearchParams(window.location.search);
    let currentUser = await getCurrentUser();

    if (urlParams.has('id')) {
        const id = urlParams.get('id');
        let goalData = await retrieveGoal(id);

        if (goalData["owner"] == currentUser.url) {
            // editGoalButton.classList.remove("hide");
            // editGoalButton.addEventListener("click", handleEditGoalButtonClick);
            // deleteGoalButton.addEventListener("click", (async (id) => await deleteGoal(id)).bind(undefined, id));
            // okGoalButton.addEventListener("click", (async (id) => await updateGoal(id)).bind(undefined, id));
            // cancelGoalButton.classList.remove("hide");
            okGoalButton.className = okGoalButton.className.replace(" hide", "");
            previousPageUrl = getReferrer()
            if (previousPageUrl.match("delete-goal")){
                okGoalButton.addEventListener("click", backToDeleteGoalsPage);
            }
            else {
                okGoalButton.addEventListener("click", backToGoalsMainPage);
            }
            
        }
    } else {
        let ownerInput = document.querySelector("#inputOwner");
        ownerInput.value = currentUser.username;
        setReadOnly(false, "#form-create-goal");
        ownerInput.readOnly = !ownerInput.readOnly;

        okGoalButton.className = okGoalButton.className.replace(" hide", "");
        cancelGoalButton.className = cancelGoalButton.className.replace(" hide", "");

        okGoalButton.addEventListener("click", async () => await createGoal());
        cancelGoalButton.addEventListener("click", backToGoalsMainPage);
    }

});