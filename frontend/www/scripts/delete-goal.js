async function fetchGoals(request) {
    let response = await sendRequest("GET", `${HOST}/api/goals/`);

    if (response.ok) {
        let data = await response.json();
        
        let goals = data.results;
        let container = document.getElementById('div-content');
        // let goalTemplate = document.querySelector("#template-goals");


        goals.forEach(goal => {
            let goalTemplate = document.querySelector("#template-goals");
            const goalAnchor = goalTemplate.content.firstElementChild.cloneNode(true);
            goalAnchor.href = `create-goal.html?id=${goal.id}`;

            const input = goalAnchor.querySelector("input");
            input.id = goal.id;
            input.value = goal.id;

            const h5 = goalAnchor.querySelector("h5");
            h5.textContent = goal.name;

            // const p = goalAnchor.querySelector("p");
            // p.textContent = goal.target;  
            
            const p = goalAnchor.getElementsByTagName('p');
            p[0].textContent = "Target to hit: " + goal.target;
            p[1].textContent = "Target date: " + goal.date;
            p[2].textContent = "Type: " + goal.typeGroup;
            
            /*
            let table = aMeal.querySelector("table");
            let rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll("td")[1].textContent = goal.typegroup; // Goal Type
            rows[1].querySelectorAll("td")[1].textContent = goal.target; // Target
            */
           
            container.appendChild(goalAnchor);
        });
    
    }


    return response;
}

function changed(termsCheckBox){
    //If the checkbox has been checked
    if(termsCheckBox.checked){
        document.getElementById("btn-delete-goal").disabled = false;
    } else{
        document.getElementById("btn-delete-goal").disabled = true;
    }
}

function getIdsToDelete(){
    //let exerciseTemplate = document.querySelector("#template-exercise");
 
     //let cbs = document.querySelectorAll('.checkbox').checked;
 
     const list_test = []
     var markedCheckbox = document.querySelectorAll('input[type="checkbox"]:checked');  
     console.log(markedCheckbox)
    
     for (i=0; i<markedCheckbox.length; i++) {  
         id = markedCheckbox[i].id
         list_test.push(id);
         deleteGoal(id);
         // deleteButton.addEventListener("click", (async (id) => await deleteExercise(id)).bind(undefined, exerciseId));
     }  
    
 
 }

async function deleteGoal(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/goals/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete goal ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("goals.html");
    }
}

function handleCancelButton(){
    window.location.replace("goals.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    let deleteButton = document.querySelector("#btn-delete-goal");
    deleteButton.addEventListener("click", getIdsToDelete);

    let cancelButton = document.querySelector('#btn-cancel-delete');
    cancelButton.addEventListener("click", handleCancelButton);

    let response = await fetchGoals();
    
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve your goals!", data);
        document.body.prepend(alert);
    }
});