let fileToAttach = "pushUp.jpg"

context('Actions', () =>{

    it('Athlete Viewing Own Workout Test', () => {
        cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

        cy.contains('Log in').click()

        cy.get("form").children().first().type("athleteTest").next().type("qwerty").next().click().next().click()
    
        cy.wait(1000)
        cy.get('#nav-workouts').click({force:true})

        cy.wait(500)
        cy.get('[id="list-my-workouts-list"]').click()

        cy.wait(500)

        cy.get("#div-content").children().last().click({force: true})

        cy.get('[name=name]').should('have.value', 'athleteWorkoutTest')
        cy.get('[name=notes]').should('have.value', 'athlete create workout test')
    })

    it('Athlete Viewing Public Workout Test', () => {
        cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

        cy.contains('Log in').click()

        cy.get("form").children().first().type("athleteTest").next().type("qwerty").next().click().next().click()
    
        cy.wait(1000)
        cy.get('#nav-workouts').click({force:true})

        cy.wait(500)
        cy.get('[id="list-public-workouts-list"]').click()

        cy.wait(500)
        cy.get("#div-content").children().first().click()

        // cy.url().should('include', '/workout.html')

        cy.get('[name=name]').should('have.value', 'ThisIsAPublicWorkout')
        cy.get('[name=notes]').should('have.value', 'etyh public workout test')
    })

    it('Coach Viewing Athlete Non-Private Workout Test', () => {
        cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

        cy.contains('Log in').click()

        cy.get("form").children().first().type("etyh").next().type("01234").next().click().next().click()
    
        cy.wait(1000)
        cy.get('#nav-workouts').click({force:true})

        cy.wait(500)
        cy.get('[id="list-athlete-workouts-list"]').click()

        cy.wait(500)
        cy.get("#div-content").children().last().click({force:true})

        // cy.url().should('include', '/workout.html')

        cy.get('[name=name]').should('have.value', 'athleteWorkoutTest')
        cy.get('[name=notes]').should('have.value', 'athlete create workout test')
    })

    it('Coach Viewing Public Workout Test', () => {
        cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

        cy.contains('Log in').click()

        cy.get("form").children().first().type("etyh").next().type("01234").next().click().next().click()
    
        cy.wait(1000)
        cy.get('#nav-workouts').click({force:true})

        cy.wait(500)
        cy.get('[id="list-public-workouts-list"]').click()

        cy.wait(500)
        cy.get("#div-content").children().first().click({force:true})

        cy.get('[name=name]').should('have.value', 'ThisIsAPublicWorkout')
        cy.get('[name=notes]').should('have.value', 'etyh public workout test')

    })

    it('Public Viewing Public Workout Test', () => {
        cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

        cy.contains('Log in').click()

        cy.get("form").children().first().type("publicTest").next().type("01234").next().click().next().click()
    
        cy.wait(1000)
        cy.get('#nav-workouts').click({force:true})

        cy.wait(500)
        cy.get('[id="list-public-workouts-list"]').click()

        cy.wait(500)
        cy.get("#div-content").children().first().click({force:true})

        cy.get('[name=name]').should('have.value', 'ThisIsAPublicWorkout')
        cy.get('[name=notes]').should('have.value', 'etyh public workout test')

    })
})