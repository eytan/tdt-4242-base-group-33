context('Login page', () => {
    beforeEach(() => {
      cy.visit('http://tdt4242-group33-frontend.herokuapp.com')
      cy.contains('Register').click()

    })

    it("[LOGIN] All fields should be filled in", () => {
    cy.get('[name="username"]')
      .should('have.attr', 'required');

    cy.get('[name="password"]')
      .should('have.attr', 'required');

    cy.get('[name="password"]')
      .should('have.attr', 'type', 'password');

    cy.get('[name="password1"]')
      .should('have.attr', 'required');

    cy.get('[name="password1"]')
      .should('have.attr', 'type', 'password');

    cy.get('[name="phone_number"]')
      .should('have.attr', 'required');

    cy.get('[name="country"]')
      .should('have.attr', 'required');

    cy.get('[name="city"]')
      .should('have.attr', 'required');

    cy.get('[name="street_address"]')
      .should('have.attr', 'required');

    })

})

const generateUsername = () =>{
    return Math.random().toString(36).substring(1,10)
}

const generateRandomString = (length) =>{
    var result = "a".repeat(length);
    return result
}

context('Boundary testing suite', () => {
    beforeEach(() => {
      cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

      cy.contains('Register').click()

    })
    //Boundary testing for Username field
    it ("[USERNAME] Accept if contains at least 1 character, within boundary", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.url().should('include', '/workouts.html')
    })

    it ("[USERNAME] Alert if field is empty, out of bounds", () => {
        cy.get("form").children().first()
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })

    it ("[USERNAME] Alert if field >150 characters, out of bounds", () => {
        cy.get("form").children().first().type("abcdefghijklmnopqrstuvwxyzabcdabcdefghijklmnopqrstuvwxyzabcdabcdefghijklmnopqrstuvwxyzabcdabcdefghijklmnopqrstuvwxyzabcdabcdefghijklmnopqrstuvwxyzabcde")
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })

    //Boundary testing for email
    it ("[EMAIL] Accept if email entered is valid, within boundary", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type(generateRandomString(5) + "@" + generateRandomString(5) + "." + generateRandomString(3))
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.url().should('include', '/workouts.html')
    })

    it ("[EMAIL] Alert if email entered is not valid, out of bounds", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abcdefghijklmnop")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })

    //Boundary testing for password
    it ("[PASSWORD] Alert if field is empty, out of bounds", () => {
        cy.get("form").children().first()
        .next().type("abc@hotmail.com")
        .next()
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })

    it ("[PASSWORD1] Alert if field is empty, out of bounds", () => {
        cy.get("form").children().first()
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next()
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })

    //Boundary testing for phone number
    it ("[PHONE NUMBER] Accept if <50 characters, within boundary", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.url().should('include', '/workouts.html')
    })

    it ("[PHONE NUMBER] Alert if >50 characters, out of bounds", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })

    //Boundary testing for country
    it ("[COUNTRY] Accept if <50 characters, within boundary", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.url().should('include', '/workouts.html')
    })

    it ("[COUNTRY] Alert if >50 characters, out of bounds", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })

    //Boundary testing for city
    it ("[CITY] Accept if <50 characters, within boundary", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.url().should('include', '/workouts.html')
    })

    it ("[CITY] Alert if >50 characters, out of bounds", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })


    //Boundary testing for address
    it ("[ADDRESS] Accept if <50 characters, within boundary", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("Herman Kragsveg 100")

        cy.get('#btn-create-account').click();
        cy.url().should('include', '/workouts.html')
    })

    it ("[ADDRESS] Alert if >50 characters, out of bounds", () => {
        const username_example = generateUsername()
        cy.get("form").children().first().type(username_example)
        .next().type("abc@hotmail.com")
        .next().type("abc123")
        .next().type("abc123")
        .next().type("9229 2291")
        .next().type("Norway")
        .next().type("Trondheim")
        .next().type("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy")

        cy.get('#btn-create-account').click();
        cy.get('.alert').should('be.visible');
    })
})
