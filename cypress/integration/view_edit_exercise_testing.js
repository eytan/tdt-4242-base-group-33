context('Actions', () => {
    beforeEach(() => {
      cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

      cy.contains('Log in').click()

      cy.get("form").children().first().type("etyh").next().type("01234").next().click().next().click()
   
      cy.wait(1000)
      cy.get('#nav-exercises').click({force:true})
      cy.get('#btn-create-exercise').click({force:true})

      cy.get("form").children().first().type("Jumping jacks")
      .next().next().type("Jumping in counts of 4")
      .next().next().next().next().next().next().get('select').select("Legs")
    })

    //Boundary testing for Unit
    it("[UNIT] Accept if >0 characters, within boundary", () => {
        cy.get("form").children().first()
        .next().next().next().next().type("1")
        .next().next().type("1")
        .next().next().type("1")

        cy.get('#btn-ok-exercise').click()
        cy.url().should('include', '/exercises.html')
    })

    it("[UNIT] Alert if field is empty, out of bounds", () => {
        cy.get("form").children().first()
        .next().next().next().next()
        .next().next().type("1")
        .next().next().type("1")

        cy.get('#btn-ok-exercise').click()
        cy.get('.alert').should('be.visible')
    })

    it("[UNIT] Alert if field >50 characters, out of bounds", () => {
        cy.get("form").children().first()
        .next().next().next().next().type("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy")
        .next().next().type("1")
        .next().next().type("1")

        cy.get('#btn-ok-exercise').click()
        cy.get('.alert').should('be.visible')
    })

    //Boundary tests for Duration
    it("[DURATION] Accept if positive integer, within boundary (e.g. 1)", () => {
        cy.get("form").children().first()
        .next().next().next().next().type("1")
        .next().next().type("1")
        .next().next().type("1")

        cy.get('#btn-ok-exercise').click()
        cy.url().should('include', '/exercises.html')
    })

    it("[DURATION] Alert if non-positive integer, out of bounds (e.g. 0)", () => {
        cy.get("form").children().first()
        .next().next().next().next().type("1")
        .next().next().type("0")
        .next().next().type("1")

        cy.get('#btn-ok-exercise').click()
        cy.get('.alert').should('be.visible')
    })

    //Boundary tests for Calories Burned
    it("[CALORIES] Accept if positive integer, within boundary (e.g. 1)", () => {
        cy.get("form").children().first()
        .next().next().next().next().type("1")
        .next().next().type("1")
        .next().next().type("1")

        cy.get('#btn-ok-exercise').click()
        cy.url().should('include', '/exercises.html')
    })

    it("[CALORIES] Alert if non-positive integer, out of bounds (e.g. 0)", () => {
        cy.get("form").children().first()
        .next().next().next().next().type("1")
        .next().next().type("1")
        .next().next().type("0")

        cy.get('#btn-ok-exercise').click()
        cy.get('.alert').should('be.visible')
    })

})