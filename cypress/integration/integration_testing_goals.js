context('Actions', () => {
    beforeEach(() => {
      cy.visit('http://tdt4242-group33-frontend.herokuapp.com')

      cy.contains('Log in').click()

      cy.get("form").children().first().type("etyh").next().type("01234").next().click().next().click()
   
      cy.wait(1000)
      cy.get('#nav-goals').click({force:true})
    })

    it("Redirecting from Goal Page To Create Goal Page Test", () => {
        cy.get('#btn-create-goal').click()

        cy.contains('Create Goal')
    })

    it('Redirecting from Create Goal to Goal Page Test', () => {
        cy.get('#btn-create-goal').click()

        cy.get('form').children().first().type('ThisIsATest')
        .next().next().type('2022-04-15T16:00')
        .next().next().next().next().type('235')
        .get('select').select("Reps")

        cy.get("#btn-ok-goal").click()
        cy.url().should('include', '/goals.html')
    })

    it('Create New Goal Test', () => {
        cy.get('#btn-create-goal').click()

        cy.get('form').children().first().type('ThisIsATest2')
        .next().next().type('2022-04-15T16:00')
        .next().next().next().next().type('235')
        .get('select').select("Reps")

        cy.get("#btn-ok-goal").click()
        cy.url().should('include', '/goals.html')
        cy.contains("h5", "ThisIsATest2")
    })

    it('Cancel Creating New Goal Test', () => {
        
        cy.get('#btn-create-goal').click()

        cy.get('form').children().first().type('ThisShouldNotBeIncluded')
        .next().next().type('2022-04-15T16:00')
        .next().next().next().next().type('235')
        .get('select').select("Reps")

        cy.get("#btn-cancel-goal").click()
        cy.url().should('include', '/goals.html')
    })

    it('Redirecting from Goals Page to Goal Detail Page Test', () => {
        cy.contains("h5", "ThisIsATest").click()

        cy.contains("Goal Details")
    })

    it("Redirecting From Goal Detail Page To Goals Page Test", () => {
        cy.contains("h5", "ThisIsATest").click()

        cy.get("#btn-ok-goal").click()
        cy.url().should('include', '/goals.html')
    })

    it("Redirecting From Goal Page To Delete Goal Page Test", () => {
        cy.get("#btn-delete-goal").click()

        cy.url().should('include', '/delete-goal.html')
    })

    it("Redirecting From Delete Goal Page To Goal Detail Page Test", () => {
        cy.get('#btn-delete-goal').click()

        cy.contains('h5', 'ThisIsATest').click()

        cy.contains("Goal Details")
    })

    it("Redirecting From Goal Detail Page To Delete Goals Page Test", () => {
        cy.get('#btn-delete-goal').click()

        cy.contains('h5', 'ThisIsATest').click()

        cy.contains("Goal Details")

        cy.get("#btn-ok-goal").click()
        cy.url().should('include', '/delete-goal.html')
    })

    it('Cancel Delete Goal/ Redirecting From Delete Goal Page To Goals Page Test', () => {
        
        cy.get('#btn-delete-goal').click()

        // cy.get('template').get('a').get('div').find('[id="19"]').check()

        cy.contains('h5', 'ThisIsATest').parent().siblings().check()

        cy.get('#btn-cancel-delete').click()
        cy.url().should('include', '/goals.html')
    })

    it('Delete Goal Test', () => {
        
        cy.get('#btn-delete-goal').click()

        // cy.get('template').get('a').get('div').find('[id="19"]').check()

        cy.contains('h5', 'ThisIsATest').parent().siblings().check()

        cy.get('#btn-delete-goal').click()
        cy.get('ThisIsATest').should('not.exist')
    })

    
  })